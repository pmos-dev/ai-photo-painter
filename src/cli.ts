import { ERgba, IRgb, IRgba, TXy } from 'tscommons-es-graphics';

import { CommonsArgs, commonsOutputPercent } from 'nodecommons-es-cli';
import {
		commonsOutputSetDebugging,
		commonsOutputDoing,
		commonsOutputSuccess
} from 'nodecommons-es-cli';
import { commonsFileWriteBinaryFile } from 'nodecommons-es-file';
import { CommonsJimp, CommonsTiffWriter } from 'nodecommons-es-graphics';
import { TCommonsAiGenericTrainingData } from 'nodecommons-es-ai';

import { Painter } from './classes/painter';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) commonsOutputSetDebugging(true);

async function doTrain(
		net: string,
		src: string,
		hiddenLayers: number[],
		iterations: number,
		duplicates: number = 3,
		subPixelNoise: number = 0.1,
		colorNoise: number = 3 / 255
): Promise<void> {
	commonsOutputDoing(`Loading image from ${src}`);
	const image: CommonsJimp = new CommonsJimp();
	await image.loadFromFile(src);
	commonsOutputSuccess();

	commonsOutputDoing(`Converting to RGB matrix`);
	const rgbs: IRgba[][] = await image.toRgbaXyMatrix();
	commonsOutputSuccess();

	commonsOutputDoing(`Building training data`);
	const trainingData: TCommonsAiGenericTrainingData<TXy, IRgb>[] = [];
	for (let x = 0; x < rgbs.length; x++) {
		for (let y = 0; y < rgbs[0].length; y++) {
			for (let attempts = duplicates; attempts-- > 0;) {
				// noise for the attempts is done in the painter class

				trainingData.push({
						input: { x: x, y: y },
						output: {
								[ ERgba.RED ]: rgbs[x][y][ERgba.RED],
								[ ERgba.GREEN ]: rgbs[x][y][ERgba.GREEN],
								[ ERgba.BLUE ]: rgbs[x][y][ERgba.BLUE]
						}
				});
			}
		}
	}
	commonsOutputSuccess();

	const painter: Painter = new Painter(
			rgbs.length,
			rgbs[0].length,
			8,	// training is always done on 8-bit images
			subPixelNoise,
			colorNoise
	);
	await painter.train(
			trainingData,
			hiddenLayers,
			iterations
	);

	await painter.save(net);
}

async function doPaint(
		net: string,
		dest: string,
		width: number,
		height: number,
		outputBitsPerSample: number
): Promise<void> {
	const painter: Painter = new Painter(
			width,
			height
	);
	await painter.load(net);

	commonsOutputDoing(`Rendering image`);
	const pixels: number[] = [];
	for (let y = 0; y < height; y++) {
		commonsOutputPercent(y, height);
		for (let x = 0; x < width; x++) {
			const pixel: IRgb = await painter.run({
					x: x,
					y: y
			});

			pixels.push(...[ pixel.red, pixel.green, pixel.blue ]);
		}
	}
	commonsOutputSuccess();

	commonsOutputDoing(`Saving result to ${dest}`);

	const tiffWriter: CommonsTiffWriter = new CommonsTiffWriter(
			pixels,
			width,
			height,
			outputBitsPerSample
	);
	tiffWriter.generate();

	commonsFileWriteBinaryFile(dest, tiffWriter.toBuffer());

	commonsOutputSuccess();
}

(async (): Promise<void> => {
	const net: string = args.getString('net');

	if (args.hasAttribute('train')) {
		const src: string = args.getString('src');
		const hiddenLayers: number[] = args.getString('hidden-layers')
				.split(',')
				.map((layer: string): number => parseInt(layer, 10));
		const iterations: number = args.getNumber('iterations');
		const duplicates: number = args.getNumberOrUndefined('pixel-duplicates') || 3;
		const subPixelNoise: number = args.getNumberOrUndefined('sub-pixel-noise') || 0.1;
		const colorNoise: number = args.getNumberOrUndefined('colour-noise') || args.getNumberOrUndefined('color-noise') || (3 / 255);

		await doTrain(
				net,
				src,
				hiddenLayers,
				iterations,
				duplicates,
				subPixelNoise,
				colorNoise
		);
	}

	if (args.hasAttribute('paint')) {
		const dest: string = args.getString('dest');
		const width: number = args.getNumber('width');
		const height: number = args.getNumber('height');
		const bitsPerSample: number = args.getNumberOrUndefined('bits-per-sample') || 8;

		await doPaint(
				net,
				dest,
				width,
				height,
				bitsPerSample
		);
	}
})();
