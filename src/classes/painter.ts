import { ERgba, IRgb, TXy } from 'tscommons-es-graphics';

import {
		commonsAiDataHumanise,
		CommonsAiNeuralNetwork,
		TCommonsAiGenericTrainingData,
		TCommonsAiTrainingData
} from 'nodecommons-es-ai';

export class Painter extends CommonsAiNeuralNetwork<TXy, IRgb> {
	constructor(
			private width: number,
			private height: number,
			private outputBitsPerSample: number = 8,
			private subPixelNoise: number = 0.1,
			private colorNoise: number = 3 / 255
	) {
		super();
	}

	protected async convertInputData(data: TXy): Promise<number[]> {
		return [
				data.x / this.width,
				data.y / this.height
		];
	}

	protected async processIn(data: TCommonsAiGenericTrainingData<TXy, IRgb>[]): Promise<TCommonsAiTrainingData[]> {
		return data
				.map((d: TCommonsAiGenericTrainingData<TXy, IRgb>): TCommonsAiTrainingData => ({
						input: [
								commonsAiDataHumanise(d.input.x / this.width, (1 / this.width) * this.subPixelNoise),	// the 1 / width is because the actual range is 0..1
								commonsAiDataHumanise(d.input.y / this.height, (1 / this.height) * this.subPixelNoise)
						],
						output: [
								commonsAiDataHumanise(d.output.red / 255, this.colorNoise),
								commonsAiDataHumanise(d.output.green / 255, this.colorNoise),
								commonsAiDataHumanise(d.output.blue / 255, this.colorNoise)
						]
				}));
	}

	protected async processOut(data: {}): Promise<IRgb> {
		const magnitude: number = Math.pow(2, this.outputBitsPerSample) - 1;

		return {
				[ ERgba.RED ]: Math.round((data['0'] as number) * magnitude),
				[ ERgba.GREEN ]: Math.round((data['1'] as number) * magnitude),
				[ ERgba.BLUE ]: Math.round((data['2'] as number) * magnitude)
		};
	}
}
